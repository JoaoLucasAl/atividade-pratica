# Respostas das questões práticas
![](https://media1.giphy.com/media/3knKct3fGqxhK/giphy.gif)

# - Questão 3   
A maneira de utilizar o código seria a mostrada abaixo, onde a e b são valores númericos que se deseja fazer a soma das partes inteiras. Caso algum dos valores não seja um número a resposta retornada será NaN e caso algum deles tenha valor decimal será descartado na soma.<p>

``` JavaScript
node calculadora.js [a] [b]
```
# - Questão 4
O commit que deve ser realizado para o arquivo calculadora.js nesse ponto é do tipo feat pelo fato de conter uma nova funcionalidade. 
<br>
O commit que deve ser realizado para o arquivo README.md é do tipo docs, com o intuito de atualizar a documentação do projeto <p>

# - Questão 5
A diferença entre os seguintes códigos é que no segundo código o comando que imprime no console a soma dos valores da variável args é atribuida à uma arrow function chamada soma e é chamada com soma() após a definição da variável args. Já no primeiro código esse comando é executado diretamente sem ser atribuido à uma variável.<p>
```
const args = process.argv.slice(2);
console.log(parseInt(args[0]) + parseInt(args[1]));
```
```
const soma = () => {
    console.log(parseInt(args[0]) + parseInt(args[1]));
};

const args = process.argv.slice(2);

soma();
```

# - Questão 6
Os erros encontrados no código por mim estavam no default do swtich case, onde o segundo parâmetro do console.log estava escrito "arg[0]" e não "args[0]".Outro erro encontrado por mim foi dentro das funções referente às operações matemáticas, onde os indexes da variável args estava errado, estavam 0 e 1, sendo que o index 0 no novo código é referente à operação matemática que se deseja realizar, corrigindo ficariam os indexes 1 e 2. De acordo com o enunciado da questão foi pedido para que o código executasse devidamente a soma e divisão, como no código não havia a divisão, fiz a implementação dela com o  resultado retornando no máximo um valor com três casas decimais.<p>

# - Questão 7
As branches adicionadas foram: devolp, documentation, feature e hotfix<p>

# - Questão 10
A maneira de utilizar o código seria a mostrada abaixo, onde a e b são valores númericos e o operador é o operador que se deseja usar para a conta referente à esses dois valores. Caso algum dos valores não seja inserido conforme o indicado o código apresentará um erro na execução. No código que João fez falta uma linha de código para que funcione que é referente à coleta dos dados pela linha de comando, sendo o comando que falta: "const args = process.argv.slice(2)". A função eval(string) utilizada no código produzido por João retorna o resultado de uma conta matemática que foi apresentada na string passada como parâmetro.<p>

``` JavaScript
node calculadora.js [a] [operador] [b]
```